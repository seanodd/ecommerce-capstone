# ecommerce-capstone

Bootstrap Capstone Project of Sean O'Donnell-Daudlin 
## Project Screen Shots

### Home Page
![Image](/images/screenshots/Home.PNG "Home Page")

### Product Page 1 - racquets
![Image](/images/screenshots/racquets.PNG "Product Page 1 - racquets")
### Product Page 2 - Apparel
![Image](/images/screenshots/Apparel.PNG "Product Page 2 - Apparel")
### Product Page 3 - Shoes
![Image](/images/screenshots/Shoes.PNG "Product Page 1 - Shoes")
### Login Page
![Image](/images/screenshots/Login.PNG "Login Page")

### Registration Page
![Image](/images/screenshots/Registration.PNG "Registration Page")

### Cart Page
![Image](/images/screenshots/Cart.PNG "Cart Page")

### Checkout Page
![Image](/images/screenshots/checkout.PNG "Checkout Page")